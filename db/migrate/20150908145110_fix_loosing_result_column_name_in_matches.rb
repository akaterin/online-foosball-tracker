class FixLoosingResultColumnNameInMatches < ActiveRecord::Migration
  def change
    rename_column :matches, :loosingResult, :loosing_result
  end
end
