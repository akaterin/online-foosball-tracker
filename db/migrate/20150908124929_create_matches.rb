class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references :winner, index: true, foreign_key: true
      t.references :looser, index: true, foreign_key: true
      t.datetime :date
      t.integer :loosingResult

      t.timestamps null: false
    end
  end
end
