class AddWinningResultToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :winning_result, :integer
  end
end
