class Match < ActiveRecord::Base

  WINNING_RESULT = 10

  belongs_to :winner, class_name: 'User'
  belongs_to :looser, class_name: 'User'

  validates :winner, presence: true
  validates :looser, presence: true
  validates :date, presence: true
  validates :winning_result, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :loosing_result, presence: true, numericality: { greater_than: 0 }
  validate :loosing_result_smaller_than_winning

  private

  def loosing_result_smaller_than_winning
    if loosing_result && winning_result && loosing_result >= winning_result
      errors.add(:loosing_result, 'should be less than winning result')
    end
  end
end
