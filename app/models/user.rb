class User < ActiveRecord::Base

  has_many :won_matches, class_name: Match, foreign_key: :winner_id, inverse_of: :winner
  has_many :lost_matches, class_name: Match, foreign_key: :looser_id, inverse_of: :looser

  mount_uploader :avatar, AvatarUploader

  validates :name, presence: true, uniqueness: true
end
