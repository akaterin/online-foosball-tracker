class MatchesStatistics

  attr_reader :played
  attr_reader :won
  attr_reader :lost
  attr_reader :sum_of_points
  attr_reader :avg_result

  def initialize(played, won, lost, sum_of_points, avg_result)
    @played = played
    @won = won
    @lost = lost
    @sum_of_points = sum_of_points
    @avg_result = avg_result
  end

end