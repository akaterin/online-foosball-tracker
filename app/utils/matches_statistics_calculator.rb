require './app/utils/matches_statistics'

class MatchesStatisticsCalculator

  def self.generate_statistics(user)
    won = user.won_matches.length
    lost = user.lost_matches.length
    played = won + lost
    points_sum = calculate_points_sum(user.won_matches, user.lost_matches)
    avg_result = calculate_avg_result(user.won_matches, user.lost_matches)
    MatchesStatistics.new(played, won, lost, points_sum, avg_result)
  end

  private
  def self.calculate_points_sum(won_matches, lost_matches)
    lost_points = lost_matches.map(&:loosing_result).inject(0, &:+)
    won_points = won_matches.map(&:winning_result).inject(0, &:+)
    lost_points + won_points
  end

  def self.calculate_avg_result(won_matches, lost_matches)
    won_factor = won_matches.length
    lost_factor = lost_matches.map{ |match| match.loosing_result.to_f / match.winning_result }.inject(0, &:+)
    played_matches = (won_matches.length + lost_matches.length)

    return 0 if played_matches == 0

    (won_factor + lost_factor) / played_matches
  end

end