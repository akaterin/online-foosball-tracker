class MatchesController < ApplicationController

  def show
    @match = Match.find(params[:id])
  end

  def new
    @match = Match.new
  end

  def create
    @match = Match.new(match_params)
    @match.winning_result = Match::WINNING_RESULT

    if @match.save
      redirect_to @match
    else
      render :new
    end
  end

  private

  def match_params
    params.require(:match).permit(:winner_id, :looser_id, :date, :loosing_result)
  end

end
