class UsersController < ApplicationController

  def index
    @users = User.all
    @statistics = generate_statistics_for_users(@users)
  end

  def show
    @user = User.includes(:won_matches, :lost_matches).find(params[:id])
    @statistics = MatchesStatisticsCalculator.generate_statistics(@user)
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      redirect_to @user
    else
      render 'edit'
    end

  end

  def destroy
    #maybe shouldn't be removed just marked inactive?
    #TODO is needed? :P
    @user = User.find(params[:id])
    @user.destroy

    redirect_to users_path

  end

  private

  def user_params
    params.require(:user).permit(:name, :avatar)
  end

  def generate_statistics_for_users(users)
    statistics = Hash.new
    users.each do |user|
      statistics[user.id] = MatchesStatisticsCalculator.generate_statistics(user)
    end
    statistics
  end

end
