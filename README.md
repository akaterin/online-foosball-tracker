# Online Foosball Tracker #

## Setting up ##

```
git clone https://akaterin@bitbucket.org/akaterin/online-foosball-tracker.git
cd online-foosball-tracker
bundle install
```
### Running in development mode ###
```
rake db:setup
rails s
```
### Running in production mode ###
```
RAILS_ENV=production bundle exec rake db:create db:schema:load
RAIS_ENV=production rake assets:precompile
RAILS_SERVE_STATIC_FILES=true RAILS_ENV=production SECRET_KEY_BASE=SuperHiperApp rails s
```

### Running tests ###
```
rspec
```

## Used technologies ##
* Ruby on Rails 4
* RSpec for testing
* Bootstrap 


## Statistics solutions ##
In the statistics view there are presented several metrics: played, won, lost games, total amout of scored point and average result factor.The last one is set as default and needs to be explained. It is calculated as the sum of scored points divided by points scored by winner, divided by number of matches played. 
So, for example, let's take Mr Hulk who played 3 matches:

Hulk vs Iron Man: 2 - 10

Hulk vs Black Widow: 7 - 10

Hulk vs Captain America 10 - 9 

His average result factor is: (2/10 + 7/10 + 10/10)/3.

## Development proposals ##

Few ideas how to make the application better :)

# Features #
* Authentication and authorisation - Right now everyone can create new users, matches. Probably it would be a good idea to add judge role who would be able to add/edit/delete matches. The users should also have the possibility to edit theirs profiles.
* Statistics - Some more complicated systems could be implemented i.e. elo rating system. 
* Fun metrics - In foosball sometimes goals, even own goals are scored in very unusual/funny way (for example if the ball firsts hits the ceiling, then somebody's forehead then... goal ;)). It will be a good idea to add extra fun-points for this kind of stuff.  

# UI #
To be honest the application right now is not super-beautiful and probably should be re-designed. Few things which come up into my mind right now:

* In add match view autocomplete should be added to winner and looser fields.
* In add match view winner and looser avatars should be displayed.
* In add match view maybe some more sophisticated spinner should be added to loosing result fields, which would not give the user ability to put incorrect value there.
* In add user view avatar preview could be added