require 'rails_helper'

describe UsersController do

  describe 'GET #index' do
    let!(:user) { FactoryGirl.create(:user) }

    it 'populates an array of users' do
      get :index
      expect( assigns(:users) ).to eq [user]
    end

    it 'populates a map with statistics' do
      get :index
      expect( assigns(:statistics) ).to have_key(user.id)
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    it 'populates user data' do
      user = FactoryGirl.create(:user)
      get :show, id: user
      expect( assigns(:user) ).to eq user
    end

    it 'populates statistic data' do
      #TODO consider if it is enough
      user = FactoryGirl.create(:user)
      get :show, id: user
      expect( assigns(:statistics) ).to be_a(MatchesStatistics)
    end

    it 'renders the :show view' do
      user = FactoryGirl.create(:user)
      get :show, id: user
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do
    it 'creates new model' do
      get :new
      expect( assigns(:user) ).to be_a_new(User)
    end

    it 'renders :new view' do
      get :new
      expect(response).to render_template :new
    end
  end

  describe 'GET #edit' do

    it 'populates user data' do
      user = FactoryGirl.create(:user)
      get :edit, id: user
      expect( assigns(:user) ).to eq user
    end

    it 'renders the :edit view' do
      user = FactoryGirl.create(:user)
      get :edit, id: user
      expect(response).to render_template :edit
    end

  end

  describe 'POST #create' do

    context 'with valid attributes' do
      it 'creates a new user' do
        expect{
          post :create, user: FactoryGirl.attributes_for(:user)
        }.to change(User,:count).by(1)
      end

      it 'redirects to the new user' do
        post :create, user: FactoryGirl.attributes_for(:user)
        expect(response).to redirect_to User.last
      end
    end

    context 'with invalid attributes' do
      it 'does not save the new user' do
        expect{
          post :create, user: FactoryGirl.attributes_for(:invalid_user)
        }.to_not change(User,:count)
      end

      it 're-renders the new method' do
        post :create, user: FactoryGirl.attributes_for(:invalid_user)
        expect(response).to render_template :new
      end
    end

  end

  describe 'PUT update' do
    let(:user) { FactoryGirl.create(:user, name: 'Nala') }

    context 'valid attributes' do
      it 'located the requested @user' do
        put :update, id: user, user: FactoryGirl.attributes_for(:user)
        expect( assigns(:user) ).to eq user
      end

      it "changes @user's attributes" do
        put :update, id: user,
            user: FactoryGirl.attributes_for(:user, name: 'Kiara')
        user.reload
        expect(user.name).to eq('Kiara')
      end

      it 'redirects to the updated user' do
        put :update, id: user, user: FactoryGirl.attributes_for(:user)
        expect(response).to redirect_to user
      end
    end

    context 'invalid attributes' do
      it 'locates the requested user' do
        put :update, id: user, user: FactoryGirl.attributes_for(:invalid_user)
        expect(assigns(:user)).to eq(user)
      end

      it "does not change user's attributes" do
        put :update, id: user,
            user: FactoryGirl.attributes_for(:user, name: nil)
        user.reload
        expect(user.name).to_not eq(nil)
      end

      it 're-renders the edit method' do
        put :update, id: user, user: FactoryGirl.attributes_for(:invalid_user)
        expect(response).to render_template :edit
      end
    end
  end


end