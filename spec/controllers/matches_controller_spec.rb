require 'rails_helper'

describe MatchesController do

  describe 'GET #show' do
    let(:match){ FactoryGirl.create(:match) }

    it 'populates user data' do
      get :show, id: match
      expect( assigns(:match) ).to eq match
    end

    it 'renders the :show view' do
      get :show, id: match
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do
    it 'creates new model' do
      get :new
      expect( assigns(:match) ).to be_a_new(Match)
    end

    it 'renders :new view' do
      get :new
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do

    context 'with valid attributes' do
      let(:winner) { FactoryGirl.create(:user, name: 'GreatUseeer') }
      let(:looser) { FactoryGirl.create(:user, name: 'NotSooGreatUseer') }
      let(:attributes) { FactoryGirl.attributes_for(:match).merge(looser_id: looser.id, winner_id: winner.id) }

      it 'creates a new match' do
        expect{
          post :create, match: attributes
        }.to change(Match,:count).by(1)
      end

      it 'redirects to the new match' do
        post :create, match: attributes
        expect(response).to redirect_to Match.last
      end
    end

    context 'with invalid attributes' do
      it 'does not save the new match' do
        expect{
          post :create, match: FactoryGirl.attributes_for(:invalid_match)
        }.to_not change(Match,:count)
      end

      it 're-renders the new method' do
        post :create, match: FactoryGirl.attributes_for(:invalid_match)
        expect(response).to render_template :new
      end
    end

  end

end