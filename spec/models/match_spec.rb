require 'rails_helper'

describe 'Match' do

  it 'has a valid factory' do
    instance = FactoryGirl.create(:match)
    expect(instance).to be_valid
  end

  it 'is invalid without winner' do
    instance = FactoryGirl.build(:match, winner: nil)
    expect(instance).not_to be_valid
  end

  it 'is invalid without looser' do
    instance = FactoryGirl.build(:match, looser: nil)
    expect(instance).not_to be_valid
  end

  it 'is invalid without date' do
    instance = FactoryGirl.build(:match, date: nil)
    expect(instance).not_to be_valid
  end

  it 'is invalid without winning result' do
    instance = FactoryGirl.build(:match, winning_result: nil)
    expect(instance).not_to be_valid
  end

  it 'is invalid if winning result negative' do
    instance = FactoryGirl.build(:match, winning_result: -2)
    expect(instance).not_to be_valid
  end

  it 'is invalid if winning result is not integer' do
  instance = FactoryGirl.build(:match, winning_result: 0.5)
  expect(instance).not_to be_valid
  end

  it 'is invalid without loosing result' do
    instance = FactoryGirl.build(:match, loosing_result: nil)
    expect(instance).not_to be_valid
  end

  it 'is invalid if loosing result greater than winning result' do
    instance = FactoryGirl.build(:match, loosing_result: 11, winning_result: 10)
    expect(instance).not_to be_valid
  end

  it 'is invalid if loosing result equals winning result' do
    instance = FactoryGirl.build(:match, loosing_result: 10, winning_result: 10)
    expect(instance).not_to be_valid
  end

end