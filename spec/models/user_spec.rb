require 'rails_helper'

describe 'User' do

  it 'has a valid factory' do
    instance = FactoryGirl.create(:user)
    expect(instance).to be_valid
  end

  it 'has many won matches' do
    t = User.reflect_on_association(:won_matches)
    expect(t.macro).to eq :has_many
  end

  it 'has many lost matches' do
    t = User.reflect_on_association(:lost_matches)
    expect(t.macro).to eq :has_many
  end

  it 'is invalid without name' do
    instance = FactoryGirl.build(:user, name: nil)
    expect(instance).not_to be_valid
  end

  it 'is invalid if name is not unique' do
    instance1 = FactoryGirl.create(:user, name: 'Ala')
    instance2 = FactoryGirl.build(:user, name: 'Ala')
    expect(instance2).not_to be_valid
  end

end