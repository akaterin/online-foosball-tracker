require 'faker'

FactoryGirl.define do
  sequence(:name) { |n| "SuperDuperHiperUser#{n}" }

  factory :user do
    name
  end

  factory :invalid_user, parent: :user  do
    name nil
  end

end