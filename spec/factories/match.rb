
FactoryGirl.define do
  factory :match do
    association :winner, factory: :user
    association :looser, factory: :user#
    date Date.new(2015,9,9)
    winning_result 10
    loosing_result 6

    factory :invalid_match do
      date nil
    end

  end

end