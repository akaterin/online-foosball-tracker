require 'rails_helper'

describe MatchesStatisticsCalculator do

  context 'no matches played' do
    let(:user) { FactoryGirl.build(:user, lost_matches: [], won_matches: []) }

    it 'generates correct number of played matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.played).to eq 0
    end

    it 'generates correct number of won matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.won).to eq 0
    end

    it 'generates correct number of lost matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.lost).to eq 0
    end

    it 'generates correct sum of points' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.sum_of_points).to eq 0
    end

    it 'generates correct avg result factor' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.avg_result).to eq 0
    end
  end

  context 'no matches won' do
    let(:number_of_matches) { 7 }
    let(:loosing_result) { 6 }
    let(:winning_result) { 10 }
    let(:matches) { FactoryGirl.build_list(:match, number_of_matches, loosing_result: loosing_result, winning_result: winning_result) }
    let(:user) { FactoryGirl.build(:user, won_matches: [], lost_matches: matches) }

    it 'generates correct number of played matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.played).to eq number_of_matches
    end

    it 'generates correct number of won matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.won).to eq 0
    end

    it 'generates correct number of lost matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.lost).to eq number_of_matches
    end

    it 'generates correct sum of points' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.sum_of_points).to eq number_of_matches*loosing_result
    end

    it 'generates correct avg result factor' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.avg_result).to eq loosing_result.to_f/winning_result
    end
  end


  context 'no matches lost' do
    let(:number_of_matches) { 7 }
    let(:loosing_result) { 6 }
    let(:winning_result) { 10 }
    let(:matches) { FactoryGirl.build_list(:match, number_of_matches, loosing_result: loosing_result, winning_result: winning_result) }
    let(:user) { FactoryGirl.build(:user, won_matches: matches, lost_matches: []) }


    it 'generates correct number of played matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.played).to eq number_of_matches
    end

    it 'generates correct number of won matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.won).to eq number_of_matches
    end

    it 'generates correct number of lost matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.lost).to eq 0
    end

    it 'generates correct sum of points' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.sum_of_points).to eq number_of_matches*winning_result
    end

    it 'generates correct avg result factor' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.avg_result).to eq 1
    end
  end

  context 'some matches won some lost' do
    let(:number_of_won_matches) { 7 }
    let(:number_of_lost_matches) { 5 }
    let(:loosing_result) { 6 }
    let(:winning_result) { 10 }
    let(:won_matches) { FactoryGirl.build_list(:match, number_of_won_matches, loosing_result: loosing_result, winning_result: winning_result) }
    let(:lost_matches) { FactoryGirl.build_list(:match, number_of_lost_matches, loosing_result: loosing_result, winning_result: winning_result) }
    let(:user) { FactoryGirl.build(:user, lost_matches: lost_matches, won_matches: won_matches) }

    it 'generates correct number of played matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.played).to eq number_of_lost_matches + number_of_won_matches
    end

    it 'generates correct number of won matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.won).to eq number_of_won_matches
    end

    it 'generates correct number of lost matches' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.lost).to eq number_of_lost_matches
    end

    it 'generates correct sum of points' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expect(result.sum_of_points).to eq number_of_won_matches*winning_result + number_of_lost_matches*loosing_result
    end

    it 'generates correct avg result factor' do
      result = MatchesStatisticsCalculator.generate_statistics(user)
      expected_avg_result = 10.to_f/12
      expect(result.avg_result).to eq expected_avg_result
    end
  end


end